# TODO : Create a s3 bucket with aws_s3_bucket

resource "aws_s3_bucket_acl" "s3_job_offer_bucket_boigoulin" {
  bucket        = "s3-job-offer-bucket-boigoulin"
  acl           = "public-read"
  force_destroy = true
  tags          = {
    Name = "Job Offer Bucket"
  }
}

# TODO : Create 1 nested folder :  job_offers/raw/  |  with  aws_s3_object
resource "aws_s3_object" "object" {
  bucket = aws_s3_bucket.s3_job_offer_bucket_boigoulin.id
  key = "job_offers/raw/"
  source = "/dev/null"
}


# TODO : Create an event to trigger the lambda when a file is uploaded into s3 with aws_s3_bucket_notification
resource "aws_s3_bucket_notification" "lambda_notification" {
  bucket = aws_s3_bucket.s3_job_offer_bucket_boigoulin.id

  lambda_function {
    lambda_function_arn = aws_lambda_function.job_processing_lambda.arn
    events              = ["s3:ObjectCreated:*"]
    filter_prefix       = "job_offers/raw/"
    filter_suffix       = ".csv"
  }

  depends_on = [aws_lambda_permission.allow_bucket]
}
